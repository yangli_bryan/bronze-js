// TODO:
//
// Implement a function called `compact`. This function creates an array with
// all falsey values removed. The values `false`, `null`, `0`, `""`, `undefined`,
// and `NaN` are falsey.
//
// You can refer to unit test for more details.
//
// <--start-
function compact (array) {
  if (array === null || array === undefined) {
    return array;
  }
  const falsey = [false, null, 0, '', undefined, NaN];
  return array.filter(value => falsey.includes(value) === false);
}
// --end-->

export default compact;
