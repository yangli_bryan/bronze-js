class RecepitCalculator {
  constructor (products) {
    this.products = [...products];
  }

  // TODO:
  //
  // Implement a method to calculate the total price. Please refer to unit test
  // to understand the requirement.
  //
  // Note that you can only modify code within the specified area.
  // <-start-
  getTotalPrice (ids) {
    let totalPrice = 0;
    for (let i = 0; i < ids.length; i++) {
      const productFound = this.products.find(product => product.id === ids[i]);
      if (productFound === undefined) {
        throw new Error('Product not found.');
      } else {
        totalPrice += productFound.price;
      }
    }
    return totalPrice;
  }
  // --end->
}

export default RecepitCalculator;
